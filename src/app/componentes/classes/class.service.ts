import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'
import { Aula } from './aula';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ClassService {

  // private readonly API = ' http://localhost:3000/cursoBB2022'

  private readonly API = 'https://apidbjson.vercel.app/cursoBB2022'

  constructor(private http: HttpClient) { }
  
  listar () : Observable<Aula[]>{
    return this.http.get<Aula[]>(this.API)
  }

  criar (aula : Aula) : Observable<Aula>{
    return this.http.post<Aula> (this.API, aula)
  }

  editar (aula : Aula) : Observable<Aula>{
    const url = `${this.API}/${aula.id}`
    return this.http.put<Aula>(url, aula)
  }

  excluir (id: string): Observable<Aula>{
    const url = `${this.API}/${id}`
    return this.http.delete<Aula>(url)
  }

  buscarPorId(id: string): Observable<Aula>{
    const url = `${this.API}/${id}`
    return this.http.get<Aula>(url)
  }
}

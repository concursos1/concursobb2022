import { Component, OnInit } from '@angular/core';
import { Aula } from '../aula';
import { ClassService } from '../class.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-editar-classes',
  templateUrl: './editar-classes.component.html',
  styleUrls: ['./editar-classes.component.css']
})
export class EditarClassesComponent implements OnInit {

  class: Aula = {
    id: '',
    materia: '',
    aula: '',
    link: [],
    material: ''
  }


  constructor(
    private service: ClassService, 
    private router: Router,
    private route: ActivatedRoute
    ) { }

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('id')
    this.service.buscarPorId((id!)).subscribe((aula) => {
      this.class = aula
    })
  }

  editarClass(){
    this.service.editar(this.class).subscribe(() => {
      this.router.navigate(['/listarAula']);
      alert("Alterado com sucesso!");
    })
  }

  cancelar(){
    this.router.navigate(['/listarAula'])
  }
}

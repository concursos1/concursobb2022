import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-list-classes',
  templateUrl: './list-classes.component.html',
  styleUrls: ['./list-classes.component.css']
})
export class ListClassesComponent implements OnInit {

  listaMaterias= [
    {id: 1, nome:  '1. Português'},
    {id: 2, nome:  '2. Inglês'},
    {id: 3, nome:  '3. Matemática'},
    {id: 4, nome:  '4. Atualidades do SFN'},
    {id: 5, nome:  '5. Matemática Financeira'},
    {id: 6, nome:  '6. Conhecimentos Bancários'},
    {id: 7, nome:  '7. Informática'},
    {id: 8, nome:  '8. Vendas e Negociação'},
  ]

  constructor() { }

  ngOnInit(): void {
  }

}
